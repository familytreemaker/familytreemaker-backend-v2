# Make a sample lambda invocation

aws --profile wsladmin \
  --region us-west-2 \
  lambda invoke \
  --function-name "FamilyTreeMakerCdkStack-FamilyTreeMakerBackendLamb-aZ2R52GiFoaG" \
  --invocation-type "RequestResponse" \
  --payload file://test.json \
  --cli-binary-format raw-in-base64-out \
  output.json
