const AWS = require('aws-sdk');

exports.main = async function (event: any, context: any) {
  try {
    console.log("Here's what I got: " + event.httpMethod);
    console.log("Here's what I got: " + event.path);
    let data = {
      "name": "person-1",
      "children": [
        {
          "name": "child-1",
          "children": [
            "child-1-1",
            "child-1-2",
          ]
        },
        {
          "name": "child-2",
          "children": [
            "child-2-1",
            "child-2-2",
          ]
        }
      ]
    }
    return {
      statusCode: 200,
      headers: {
        // Get requests also need these headers to allow cross-origin
        "Access-Control-Allow-Headers": "Content-Type",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
      },
      body: JSON.stringify(data),
    };
  } catch (error) {
    var body = JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: JSON.stringify(body)
    }
  }
}
