# Welcome to FamilyTreeMaker

This is the backend repository for family tree maker

# How to use?

We first need to deploy to an AWS Account.

Follow https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html to setup creds

[Recommended] Use STS to get temporary credentials
https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp_request.html

# Build and Deploy

Open a terminal

```bash
npm run build # Typescript build
cdk synth # Synthesizes cloudformation templates
cdk deploy # Deploys stack to your AWS account
```

## Other Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
