import { aws_cloudfront, aws_cloudfront_origins, aws_lambda, aws_s3, aws_s3_deployment, Duration, Stack, StackProps, CfnOutput } from 'aws-cdk-lib';
import * as apigateway from "aws-cdk-lib/aws-apigateway";

import { Construct } from 'constructs';

export class FamilyTreeMakerCdkStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // ------------------------------------------------
    // Website - React App

    const AssetsBucket = new aws_s3.Bucket(this, "FamilyTreeMakerWebsiteAssets", {
      versioned: true,
      blockPublicAccess: aws_s3.BlockPublicAccess.BLOCK_ALL,
    });

    const CloudfrontDistribution = new aws_cloudfront.Distribution(this, "FamilyTreeMakerCDN", {
      defaultBehavior: {
        origin: new aws_cloudfront_origins.S3Origin(AssetsBucket),
        allowedMethods: aws_cloudfront.AllowedMethods.ALLOW_ALL,
        viewerProtocolPolicy: aws_cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS
      },
      defaultRootObject: "index.html",
      enabled: true,
    });

    const AssetsDeployment = new aws_s3_deployment.BucketDeployment(this, "FamilyTreeMakerWebsiteDeployment", {
      sources: [aws_s3_deployment.Source.asset("../familytreemakerweb/build")],
      destinationBucket: AssetsBucket,
      distribution: CloudfrontDistribution,
      distributionPaths: ["/*"],
    });

    // ------------------------------------------------


    // ------------------------------------------------
    // API Gateway and Lambda
    //      /home -> Website
    //      /api  -> API calls
    const LambdaFunction = new aws_lambda.Function(this, "FamilyTreeMakerBackendLambda", {
      runtime: aws_lambda.Runtime.NODEJS_16_X,
      handler: "index.main",
      code: aws_lambda.Code.fromAsset(__dirname + "/../lambda"),
      timeout: Duration.minutes(5),
    });

    const api = new apigateway.LambdaRestApi(this, "familytreemaker-api", {
      handler: LambdaFunction,
      proxy: false
    });

    const familyTreeAPI = api.root.addResource('api', {
      // Preflight OPTIONS requests go through API GW and NOT Lambda.
      // See the index.ts on lambda for example
      defaultCorsPreflightOptions: {
        allowOrigins: ["https://" + CloudfrontDistribution.distributionDomainName],
        allowHeaders: ["Content-Type"],
        allowMethods: ["OPTIONS","POST","GET"]
      }
    });
    familyTreeAPI.addMethod('GET');

    const cloudfrontDistributionUrl = new CfnOutput(this, 'cloudfrontURL', {
      value: CloudfrontDistribution.distributionDomainName,
      description: 'The cloudfront distribution URL',
      exportName: 'cloudfrontURL',
    });
  }
}
